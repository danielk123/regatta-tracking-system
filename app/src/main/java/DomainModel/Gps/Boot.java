package DomainModel.Gps;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;

import java.io.Serializable;

import DomainModel.Map.Map;
import Infrastructur.CONSTANT;

/**
 * Subclass of GpsSender.
 * Models a Boot which participates in the competition.
 * Implements methods of DrawInterface to make Boot drawable.
 */
public class Boot extends GpsSender implements Serializable {

    /**
     * Constructor
     *
     * @param color Sets color of Boot. Boot will be drawn in this color.
     * @param no    Name of boot or device number
     * @param snr   sailing number
     * @param dev   device number
     */
    public Boot(int color, String no, String snr, int dev) {
        super(color, no, snr, dev);
    }

    /**
     * Calculates posIndex which is the array index of the data to the given gpsTime.
     * Delegates to drawBoot()
     * @param map Map on which the boot should be drawn.
     * @param gpsTime Time of the position.
     * @param c Canvas which draws the boot.
     */
    @Override
    public void drawPosition(Map map, long gpsTime, Canvas c) {
        int posIndex = this.gpsTime.indexOf(gpsTime);

        if (this.gpsTime.contains(gpsTime))
            drawBoot(map, c, posIndex);
        else {
            for (int j = 1; j < CONSTANT.repeats; j++) {
                posIndex = this.gpsTime.indexOf(gpsTime - j * 1000);
                if (posIndex >= 0) {
                    drawBoot(map, c, posIndex);
                    break;
                }
            }
        }
    }

    /**
     * Draws boot on a given position and rotations it according to it's cog value.
     * @param map Map on which the boot should be drawn.
     * @param c Canvas which draws the boot.
     * @param posIndex
     */
    private void drawBoot(Map map, Canvas c, int posIndex) {

        int x = map.toXCoordinateFrom(this.posLon.get(posIndex));
        int y = map.toYCoordinateFrom(this.posLat.get(posIndex));

        Paint p = new Paint();
        p.setColor(this.color);
        p.setStrokeWidth(p.getStrokeWidth() * 10);
        p.setStyle(Paint.Style.FILL);

        Path path = drawBootLines(x, y);

        Matrix transformMatrix = new Matrix();
        transformMatrix.postRotate(this.course.get(posIndex).floatValue(), x, y);
        path.transform(transformMatrix);

        c.drawPath(path, p);
    }

    /**
     * Creates the path of boat around the position on the map.
     * @param x x coordinate on the map
     * @param y y coordinate on the map
     * @return Returns the created path around the position on the map.
     */
    private Path drawBootLines(int x, int y) {
        Path path = new Path();
        path.moveTo(x - 12, y + 21);
        path.lineTo(x - 12, y - 12);
        path.lineTo(x, y - 21);
        path.lineTo(x + 12, y - 12);
        path.lineTo(x + 12, y + 21);
        return path;
    }

    /**
     * Implemented method of DrawInterface.
     * Public method to draw tail of boat.
     * Calculates posIndex at the given gpsTime or posIndex-1 if gpsTime is not registered
     * @param gpsTime recorded time of the Position
     * @param map Map on which the tail is drawn.
     * @param c Canvas which draws the tail on a bitmap.
     * @param tail length of the tail.
     */
    @Override
    public void drawTailTill(long gpsTime, Map map, Canvas c, double tail) {
        if (tail == 0 | tail == 1) return;

        Paint p = new Paint();
        p.setColor(this.color);
        p.setStrokeWidth(3);

        int tailLength;
        int posIndex = this.gpsTime.indexOf(gpsTime);

        if (this.gpsTime.contains(gpsTime)) {
            tailLength = calcTailLength(posIndex, tail);
            drawTail(map, posIndex, tailLength, c, p);
            return;
        }

        for (int j = 1; j <= 35; j++) {
            posIndex = this.gpsTime.indexOf(gpsTime - j * 1000);
            if (posIndex >= 0) {
                tailLength = calcTailLength(posIndex, tail);
                drawTail(map, posIndex, tailLength, c, p);
                break;
            }
        }
    }

    /**
     * Draws tail from posIndex subtracted by tailLength till posIndex
     * @param map
     * @param posIndex ArrayIndex of the current time (or of the last received time if there is no time)
     * @param tailLength Length of the tail.
     * @param c Canvas which draws the tail on a bitmap.
     * @param p Paint which defines drawing settings.
     */
    private void drawTail(Map map, int posIndex, int tailLength, Canvas c, Paint p) {
        for (int i = tailLength; i < posIndex; i++) {
            try {
                c.drawLine(map.toXCoordinateFrom(this.posLon.get(i)), map.toYCoordinateFrom(this.posLat.get(i)),
                        map.toXCoordinateFrom(this.posLon.get(i + 1)), map.toYCoordinateFrom(this.posLat.get(i + 1)), p);
            } catch (IndexOutOfBoundsException e) {
                Log.d("DRAWBACKGROUNDSERVICE", "IndexOutOfBoundsException:");
                Log.d("DRAWBACKGROUNDSERVICE", "Bootnumber: " + this.name);
                Log.d("DRAWBACKGROUNDSERVICE", "Array size: " + this.posLon.size());
                e.printStackTrace();
            }
        }
    }

    /**
     * Calculates tailLength. Makes sure tailLength has a valid length.
     * @param posIndex  ArrayIndex of the current time
     * @param tail tail value defined by user.
     * @return returns valid tail length.
     */
    private int calcTailLength(int posIndex, double tail) {
        int tailLength = (int) (posIndex - tail);
        if (tailLength < 0) tailLength = 0;
        if (tailLength > posIndex) tailLength = posIndex;
        return tailLength;
    }
}
