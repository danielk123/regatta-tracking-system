package DomainModel.Gps;

import java.util.ArrayList;

/**
 * Abstract class to model objects which send GpsSignals.
 * Contains lists of lat, lon, course,...
 * Implements DrawInterface so subclasses can be drawn.
 */
public abstract class GpsSender implements DrawInterface {

    public int deviceNumber;
    public String name, sailingNumber;
    public ArrayList<Double> posLat, posLon, cog, sog, course;
    public int color;
    ArrayList<Long> gpsTime;

    /**
     * Constructor. Subclasses can call this Constructor.
     *
     * @param color Sets color of object. Object will be drawn in this color.
     * @param no    Name or device number
     * @param snr   sailing number
     * @param dev   device number
     */
    public GpsSender(int color, String no, String snr, int dev) {
        deviceNumber = dev;
        sailingNumber = snr;
        name = no;
        course = new ArrayList<>();
        posLon = new ArrayList<>();
        posLat = new ArrayList<>();
        cog = new ArrayList<>();
        sog = new ArrayList<>();
        gpsTime = new ArrayList<>();
        this.color = color;
    }

    /**
     * Splits data of GpsSignal in multiple values and adds them to the specific list.
     * @param s GpsSignal which contains a new position.
     */
    public void newPosition(GpsSignal s) {
        posLat.add(s.lat);
        posLon.add(s.lon);
        gpsTime.add(s.getGPStime());
        course.add(s.cog);
    }
}
