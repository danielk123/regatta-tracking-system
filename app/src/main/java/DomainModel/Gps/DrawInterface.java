package DomainModel.Gps;

import android.graphics.Canvas;

import DomainModel.Map.Map;

/**
 * Interface to make subclasses of GpsSender drawable on the bitmap.
 * Enables different implementations for subclasses.
 */
public interface DrawInterface {
    void drawPosition(Map map, long gpsTime, Canvas c);

    void drawTailTill(long gpsTime, Map map, Canvas c1, double tail);
}
