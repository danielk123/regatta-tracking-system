package DomainModel.Gps;

import android.graphics.Color;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Enum which defines color for GpsSender
 */
public enum BootColor {
    RED(Color.RED),
    GREEN(Color.GREEN),
    BLACK(Color.BLACK),
    YELLOW(Color.YELLOW),
    GRAY(Color.GRAY),
    ORANGE(Color.rgb(255, 140, 0)),
    BROWN(Color.rgb(165, 42, 42)),
    PURPLE(Color.rgb(128, 0, 128)),
    DARKGREEN(Color.rgb(0, 100, 0)),
    DARKBLUE(Color.rgb(0, 0, 139)),
    WHITE(Color.WHITE);

    private static final List<BootColor> VALUES =
            Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();
    private int color;

    private BootColor(int color) {
        this.color = color;
    }

    /**
     * Static method to return a random color out the enum.
     *
     * @return Returns a random color of the enum.
     */
    public static int randomColor() {
        return VALUES.get(RANDOM.nextInt(SIZE)).color;
    }

    /**
     * Static method which returns a fixed color for all buoys.
     * @return Returns color for buoys.
     */
    public static int buoyColor() {
        return DARKBLUE.color;
    }
}
