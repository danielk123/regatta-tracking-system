package DomainModel.Gps;

import java.io.IOException;
import java.util.Calendar;
import java.util.IllegalFormatException;

//import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 * Models a GpsSignal which was send from a GpsSender and is received over the internet
 * or from a logfile
 */
public class GpsSignal implements Comparable<GpsSignal> {
    public int device, version;
    double lat, lon, sog, cog, rssi;
    private Calendar t;
    private int year, month, day, hour, minute, millisecs;
    private int syshour, sysminute, sysmillisecs;
    private long gpstime;

    /**
     * Constructor. Creates a GpsSignal with default values.
     *
     * @throws IOException
     * @throws IllegalFormatException
     */
    public GpsSignal() throws IOException, IllegalFormatException {
        t = Calendar.getInstance();
        year = t.get(Calendar.YEAR);
        month = t.get(Calendar.MONTH) + 1;
        day = t.get(Calendar.DAY_OF_MONTH);
        syshour = t.get(Calendar.HOUR_OF_DAY);
        sysminute = t.get(Calendar.MINUTE);
        sysmillisecs = t.get(Calendar.MILLISECOND) + 1000 * t.get(Calendar.SECOND);

        device = 0;
        hour = 0;
        minute = 0;
        millisecs = 0;
        lat = 0;
        lon = 0;
        sog = 0;
        cog = 0;
        rssi = 0;
        gpstime = 0;
    }

    /**
     * Used to interpret gps_dara byte wise.
     * @param gps_data GpsSignal as a byte Array.
     * @throws IOException
     */
    public void interpret_data(byte[] gps_data) throws IOException {
        int ii;
        ii = 0;
        version = gps_data[ii];
        ii = ii + 1;
        //device=((gps_data[ii]*256+gps_data[ii+1])*256+gps_data[ii+2])*256+gps_data[ii+3];
        device = ((gps_data[ii] & 0xff) << 24) | ((gps_data[ii + 1] & 0xff) << 16) |
                ((gps_data[ii + 2] & 0xff) << 8) | (gps_data[ii + 3] & 0xff);       // extrahiert die device ID
        ii = ii + 4;
        hour = gps_data[ii] & 0xff;     // extrahiert die stunden, minuten, sekunden
        ii = ii + 1;
        minute = gps_data[ii] & 0xff;
        ii = ii + 1;
        //millisecs=gps_data[ii]*256+gps_data[ii+1];
        millisecs = ((gps_data[ii] & 0xff) << 8) | (gps_data[ii + 1] & 0xff);
        ii = ii + 2;
        //lat=((gps_data[ii]*256+gps_data[ii+1])*256+gps_data[ii+2])*256+gps_data[ii+3];
        lat = (double) ((int) ((gps_data[ii] & 0xff) << 24) | ((gps_data[ii + 1] & 0xff) << 16) |
                ((gps_data[ii + 2] & 0xff) << 8) | (gps_data[ii + 3] & 0xff));
        lat = lat / 1.e7;
        ii = ii + 4;
        //lon=((gps_data[ii]*256+gps_data[ii+1])*256+gps_data[ii+2])*256+gps_data[ii+3];
        lon = (double) ((int) ((gps_data[ii] & 0xff) << 24) | ((gps_data[ii + 1] & 0xff) << 16) |
                ((gps_data[ii + 2] & 0xff) << 8) | (gps_data[ii + 3] & 0xff));
        lon = lon / 1.e7;
        ii = ii + 4;
        sog = ((gps_data[ii] & 0xff) << 8) | (gps_data[ii + 1] & 0xff);
        sog = sog / 100.0;
        ii = ii + 2;
        cog = ((gps_data[ii] & 0xff) << 8) | (gps_data[ii + 1] & 0xff);
        cog = cog / 100.0;
        ii = ii + 6;
        rssi = -gps_data[ii];
        gpstime = 60000L * (minute + 60L * (hour)) + millisecs;
    }

    /**
     * Sets parameters of the object according to a string array.
     * @param l String array which contains the information of the GpsSignal
     */
    public void setFromLog(String[] l) {
        int ii = 0;
        device = Integer.parseInt(l[ii++]);
        lat = Double.parseDouble(l[ii++].replace(",", "."));
        lon = Double.parseDouble(l[ii++].replace(",", "."));
        hour = Integer.parseInt(l[ii++]);
        minute = Integer.parseInt(l[ii++]);
        millisecs = Integer.parseInt(l[ii++]);
        syshour = Integer.parseInt(l[ii++]);
        sysminute = Integer.parseInt(l[ii++]);
        sysmillisecs = Integer.parseInt(l[ii++]);
        sog = Double.parseDouble(l[ii++]);
        cog = Double.parseDouble(l[ii++]);
        rssi = Double.parseDouble(l[ii++]);
        gpstime = 60000 * (minute + 60 * (hour)) + millisecs;
    }

    /**
     * Method to compare this GpsSignal with another GpsSignal by the device number.
     * @param o other GpsSignal.
     * @return
     */
    @Override
    public int compareTo(GpsSignal o) {
        return (this.device < o.device ? -1 :
                this.device == o.device ? 0 : 1);
    }

    public long getGPStime() {
        return gpstime;
    }
}


