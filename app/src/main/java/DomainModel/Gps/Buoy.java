package DomainModel.Gps;

import android.graphics.Canvas;
import android.graphics.Paint;

import DomainModel.Map.Map;
import Infrastructur.CONSTANT;
import Infrastructur.Communication.SharedData;
/**
 * Subclass of GpsSender.
 * Models a buoy for the competition.
 * Implements methods of DrawInterface to make buoy drawable on bitmap
 */
public class Buoy extends GpsSender {
    boolean isStart;

    /**
     * Constructor
     *
     * @param color   Sets color of buoy. Buoy will be drawn in this color.
     * @param no      Name or device number
     * @param snr     sailing number or device number
     * @param dev     device number
     * @param isStart Indicates if this buoy is the start/finish line
     */
    public Buoy(int color, String no, String snr, int dev, boolean isStart) {
        super(color, no, snr, dev);
        this.isStart = isStart;
    }

    /**
     * Implemented method from DrawInterface.
     * Public method to draw a buoy on a specific position.
     * Calculates posIndex which is the array index of the data to the given gpsTime.
     * @param map Map on which the boot should be drawn.
     * @param gpsTime Time of the position.
     * @param c Canvas which draws the boot.
     */
    @Override
    public void drawPosition(Map map, long gpsTime, Canvas c) {
        int posIndex = this.gpsTime.indexOf(gpsTime);

        if (this.gpsTime.contains(gpsTime))
            drawBuoy(map, c, posIndex);
        else {
            for (int j = 1; j < CONSTANT.repeats; j++) {
                posIndex = this.gpsTime.indexOf(gpsTime - j * 1000);
                if (posIndex >= 0) {
                    drawBuoy(map, c, posIndex);
                    break;
                }
            }
        }
    }

    /**
     * Does nothing at the moment since buoys don't have tails
     * @param gpsTime
     * @param map
     * @param c1
     * @param tail
     */
    @Override
    public void drawTailTill(long gpsTime, Map map, Canvas c1, double tail) {
        //do nothing since buoys are not supposed to have a tail (at the moment)
    }

    /**
     * Draws buoy on a given position.
     * @param map Map on which the buoy should be drawn.
     * @param c Canvas which draws the buoy.
     * @param posIndex Array index of the position.
     */
    private void drawBuoy(Map map, Canvas c, int posIndex) {
        int x = map.toXCoordinateFrom(this.posLon.get(posIndex + 1));
        int y = map.toYCoordinateFrom(this.posLat.get(posIndex + 1));

        Paint p = new Paint();
        p.setColor(this.color);
        p.setStrokeWidth(p.getStrokeWidth() * 10);
        p.setStyle(Paint.Style.FILL);
        drawBuoyLines(x, y, c, p);

        if (this.isStart) {
            int x2 = map.toXCoordinateFrom(SharedData.endCoordsFinishLine[1]);
            int y2 = map.toYCoordinateFrom(SharedData.endCoordsFinishLine[0]);
            drawBuoyLines(x2, y2, c, p);
            c.drawLine(x, y, x2, y2, p);
        }
    }

    /**
     * Draws the bouy around the position on the map.
     * @param x x coordinate on the map
     * @param y y coordinate on the map
     * @param c Canvas which draws the buoy on a bitmap.
     * @param p Paint which defines drawing settings.
     */
    private void drawBuoyLines(int x, int y, Canvas c, Paint p) {
        p.setStrokeWidth(3);
        c.drawCircle(x, y, 8, p);
        c.drawLine(x, y, x, y - 14, p);
        c.drawLine(x, y - 14, x + 14, y - 14, p);
        c.drawLine(x + 14, y - 14, x + 14, y + 14, p);
        c.drawLine(x + 14, y + 14, x - 14, y + 14, p);
        c.drawLine(x - 14, y + 14, x - 14, y - 14, p);
    }

}
