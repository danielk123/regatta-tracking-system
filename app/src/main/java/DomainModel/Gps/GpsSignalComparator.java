package DomainModel.Gps;

import java.util.Comparator;

/**
 * Comperator to compare GpsSignals by gpsTime.
 * If the gps time is equal they are compared by their device number
 */
public abstract class GpsSignalComparator implements Comparator<GpsSignal> {

    /**
     * Sorts GpsSignals by their gpsTime.
     * Not used at the moment.
     *
     * @param s1 GpsSignal1
     * @param s2 GpsSignal2
     * @return s1<s2 -> -1 s1>s2 1 s1==s2 0
     */
    public int compare(GpsSignal s1, GpsSignal s2) {
        if (s1.getGPStime() < s2.getGPStime()) {
            return -1;
        } else if (s1.getGPStime() > s2.getGPStime()) {
            return 1;
        } else if (s1.device > s2.device) {
            return 1;
        } else if (s1.device < s2.device) {
            return -1;
        } else {
            return 0;
        }
    }
}
