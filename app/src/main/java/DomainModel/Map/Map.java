package DomainModel.Map;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import DomainModel.Gps.GpsSender;
import Infrastructur.CONSTANT;
import kopfdaniel.regattatrackingsystem.R;

/**
 * Models the map shown to the user.
 */
public class Map {
    private static double xMinAsLon, yMinAsLat;
    private static int xTileWidth, yTileWidth;
    private static Resources resources;
    private double xMaxAsLon, yMaxAsLat;
    private Bitmap mutableBitmap;
    private Bitmap emptyMutableBitmap;
    private int xTile, yTile;
    private double lat;
    private double lon;
    private int zoom;

    /**
     * Constructur.
     * Creates Map with default values.
     *
     * @param resources Resource directory. Used to get bitmaps in ressources.
     */
    public Map(Resources resources) {
        xTileWidth = CONSTANT.xTiles;
        yTileWidth = CONSTANT.yTiles;
        this.resources = resources;

        xMaxAsLon = 8.503417969;

        double n = Math.PI - (2.0 * Math.PI * 5613) / Math.pow(2.0, 14);
        yMaxAsLat = Math.toDegrees(Math.atan(Math.sinh(n)));
    }

    /**
     * Calculates a latitude out of the y tile number and zoom
     * @param y y tile number
     * @param z zoom
     * @return Returns a latitude value for the given tile
     */
    private static double tileToLat(int y, int z) {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }

    /**
     * Calculates a longitude out of the x tile number and zoom
     * @param x x tile number
     * @param z zoom
     * @return Returns a longitude value for the given tile
     */
    private static double tileToLon(int x, int z) {
        return x / Math.pow(2.0, z) * 360.0 - 180.0;
    }

    /**
     * Sets mutable Bitmap as a copy of given bm
     * @param bm Bitmap which should be copied.
     */
    public void copyBitmap(Bitmap bm) {

        if (mutableBitmap != null)
            if (mutableBitmap.equals(bm)) return; //mutableBitmap;

        mutableBitmap = bm.copy(Bitmap.Config.ARGB_8888, true);
        //return mutableBitmap;
    }

    /**
     * Delegates drawing of the next position of the participating boats.
     * Resets emptyMutableBitmap to discharge drawings of the last position.
     * @param participatingBoots Participating boats and buoys.
     * @param gpsTime Time for which the position should be drawn.
     * @param tail Length of the Tail
     * @throws RuntimeException Should never be thrown.
     */
    public void drawNewPositions(ArrayList<GpsSender> participatingBoots, long gpsTime, double tail) throws RuntimeException {

        if (mutableBitmap == null || mutableBitmap.isRecycled()) {
            emptyMutableBitmap = Bitmap.createBitmap(CONSTANT.xTiles * 256, CONSTANT.yTiles * 256, Bitmap.Config.ARGB_8888);
            return;
        }

        emptyMutableBitmap = mutableBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas c1 = new Canvas(emptyMutableBitmap);

        for (int i = 0; i < participatingBoots.size(); i++) {
            GpsSender sender = participatingBoots.get(i);
            sender.drawPosition(this, gpsTime, c1);
            sender.drawTailTill(gpsTime, this, c1, tail);
        }
        //return emptyMutableBitmap;
    }

    /**
     * Conversion of a longitude in a x coordinate on the map .
     * @param lon Longitude
     * @return returns x coordinate.
     */
    public int toXCoordinateFrom(double lon) {
        return (int) ((lon - xMinAsLon) / (xMaxAsLon - xMinAsLon) * mutableBitmap.getWidth());
    }

    /**
     * Conversion of a latitude in a y coordinate on the map.
     * @param lat Latitude
     * @return returns y-coordinate.
     */
    public int toYCoordinateFrom(double lat) {
        return (int) ((lat - yMinAsLat) / (yMaxAsLat - yMinAsLat) * mutableBitmap.getHeight());
    }

    /**
     * Calculates the outer coordinates of the map as longitude and latitude.
     * Returns them as a double array.
     * @return coordinates.
     */
    public double[] getCoordinates() {
        double[] coords = new double[4];
        coords[0] = tileToLat(yTile, zoom);
        coords[1] = tileToLon(xTile, zoom);
        coords[2] = tileToLat(yTile + yTileWidth, zoom);
        coords[3] = tileToLon(xTile + xTileWidth, zoom);
        return coords;
    }

    /**
     * Sets min and max values of the map as latitude longitude
     * @param coords Array with the 4 coordinates. y_min, x_min, y_max, x_max
     */
    private void setCoordinates(double[] coords) {
        yMinAsLat = coords[0];
        xMinAsLon = coords[1];
        yMaxAsLat = coords[2];
        xMaxAsLon = coords[3];
    }

    /**
     * Initializes map by setting the coordinates from the logfile
     * and calculating the boundings of the map.
     * @param mapCoords lat, lon from configfile
     * @param zoom zoomlevel
     */
    public void setupMap(double[] mapCoords, int zoom) {
        lat = mapCoords[0];
        lon = mapCoords[1];
        this.zoom = zoom;
        calcTileNumber(lat, lon, zoom);

        double coords[] = getCoordinates();
        setCoordinates(coords);
    }

    /**
     * Calculates the tile number out of a latitude, longitude and zoom.
     * Sets xTile and yTile whiche are the tile numbers in the specific direction.
     * @param lat
     * @param lon
     * @param zoom
     */
    private void calcTileNumber(final double lat, final double lon, final int zoom) {
        xTile = (int) Math.floor((lon + 180) / 360 * (1 << zoom));
        yTile = (int) Math.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1 << zoom));
        if (xTile < 0) {
            xTile = 0;
        }
        if (xTile >= (1 << zoom)) {
            xTile = ((1 << zoom) - 1);
        }
        if (yTile < 0) {
            yTile = 0;
        }
        if (yTile >= (1 << zoom)) {
            yTile = ((1 << zoom) - 1);
        }
    }

    /**
     * Changes the map by shifting or changing the zoom level.
     * If the zoom level is changed, the map is shifted to stay in focus.
     * @param dx change x direction
     * @param dy change in y direction
     * @param dz change of zoom
     */
    public void changeSector(int dx, int dy, int dz) {
        if (dz != 0) {
            lon = calcCenterLon(xTile, getxTileWidth(), zoom);
            lat = calcCenterLat(yTile, getyTileWidth(), zoom);
            zoom += dz;

            calcTileNumber(lat, lon, zoom);
            if (dz > 0) {
                xTile = xTile - (getxTileWidth() * 3 / 4);
                yTile = yTile - (getyTileWidth() * 3 / 4);
            } else {
                xTile = xTile - (getxTileWidth() * 3 / 8);
                yTile = yTile - (getyTileWidth() * 3 / 8);
            }
        }
        xTile += dx;
        yTile += dy;

        setCoordinates(getCoordinates());
        Log.d("CHANGESECTOR", "changed: zoom = " + zoom + ", xTile = " + xTile + ", yTile = " + yTile);
    }

    /**
     * Calculates the longitude of the center of a tile
     * @param xt
     * @param xw
     * @param z
     * @return longitude
     */
    private double calcCenterLon(int xt, int xw, int z) {
        double lon1, lon2;
        lon1 = tile2lon(xt, z);
        lon2 = tile2lon(xt + xw, z);
        return (lon1 + lon2) / 2.0;
    }

    /**
     * Calculates the latitude of the center of a tile
     * @param yt
     * @param yw
     * @param z
     * @return latitude
     */
    private double calcCenterLat(int yt, int yw, int z) {
        double lat1, lat2;
        lat1 = tile2lat(yt, z);
        lat2 = tile2lat(yt + yw, z);
        return (lat2 + lat1) / 2.0;
    }

    /**
     * Calculates the longitude of a tile
     * @param x
     * @param z
     * @return
     */
    private double tile2lon(int x, int z) {
        return x / Math.pow(2.0, z) * 360.0 - 180.0;
    }

    /**
     * calculates the latitude of a tile
     * @param y
     * @param z
     * @return
     */
    private double tile2lat(int y, int z) {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }


    public int getxTileWidth() {
        return xTileWidth;
    }

    public int getyTileWidth() {
        return yTileWidth;
    }

    /**
     * Builds the bitmap out of multiple tiles
     * @param zoom
     * @return Bitmap
     */
    public Bitmap generateBitmap(int zoom) {

        if (mutableBitmap != null) {
            mutableBitmap.recycle();
        }

        Bitmap bm = Bitmap.createBitmap(CONSTANT.xTiles * 256, CONSTANT.yTiles * 256, Bitmap.Config.ARGB_8888);

        try {
            int test = 13;
            int test2 = zoom - test;

            Canvas c = new Canvas(bm);
            Paint p = new Paint();

            if (test2 == 1) {
                String filePath = CONSTANT.TARGET_BASE_PATH + "Images/" + zoom + "/" + (xTile) + "/" + (yTile) + ".png";
                Bitmap sectorBitmap = BitmapFactory.decodeFile(filePath);
                c.drawBitmap(Bitmap.createScaledBitmap(sectorBitmap, CONSTANT.xTiles * 256, CONSTANT.yTiles * 256, false), 0, 0, p);
                sectorBitmap.recycle();
                return bm;
            }

            for (int i = 0; i < CONSTANT.xTiles; i++) {
                for (int j = 0; j < CONSTANT.yTiles; j++) {
                    String filePath = CONSTANT.TARGET_BASE_PATH + "Images/" + zoom + "/" + (xTile + i) + "/" + (yTile + j) + ".png";
                    if (new File(filePath).exists()) {
                        Bitmap sectorBitmap = BitmapFactory.decodeFile(filePath);
                        c.drawBitmap(sectorBitmap, i * 256, j * 256, p);
                        sectorBitmap.recycle();
                        continue;
                    }
                    Bitmap dummyBitmap = BitmapFactory.decodeResource(resources, R.drawable.dummy);
                    c.drawBitmap(dummyBitmap, i * 256, j * 256, p);
                    dummyBitmap.recycle();
                }
            }
            return bm;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bm;
    }

    public Bitmap getEmptyMutableBitmap() {
        return emptyMutableBitmap;
    }

    public void setEmptyMutableBitmap(Bitmap emptyMutableBitmap) {
        this.emptyMutableBitmap = emptyMutableBitmap;
    }

    /**
     * Recycles mutableBitmap and emptyMutableBitmap to save Memory.
     */
    public void recycle() {
        emptyMutableBitmap.recycle();
        mutableBitmap.recycle();
    }
}

