package kopfdaniel.regattatrackingsystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;

import Infrastructur.CONSTANT;
import Infrastructur.ConfigReader;
import Infrastructur.FileDialog;
import Infrastructur.LogReader;

/**
 * Activity used to let the user configure replay mode by setting configfile and logfile
 * and parameters time, speed and tail
 */
public class ReplayActivity extends AppCompatActivity {

    protected TextView labelConfigFile;
    protected TextView labelLogFile;
    protected ImageView configErrorImage;
    protected ImageView logErrorImage;
    protected ImageButton addConfigImageButton;
    protected ImageButton addLogImageButton;
    protected SeekBar seekBarSpeed;
    protected SeekBar seekBarTime;
    protected SeekBar seekBarTail;
    protected Button bottonImageButton_play;

    private Boolean logIsValidated = false;
    private Boolean configIsValidated = false;

    /**
     * Called when Activity gets created
     * Initializes View objects from layout file.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_replay);

        labelConfigFile = (TextView) findViewById(R.id.labelConfigFile);
        //labelConfigFile.setText("/sdcard/Breitenau_conf.xls");

        labelLogFile = (TextView) findViewById(R.id.labelLogFile);
        //labelLogFile.setText("/sdcard/Breitenau_Replay.csv");

        configErrorImage = (ImageView) findViewById(R.id.imageView);
        logErrorImage = (ImageView) findViewById(R.id.imageViw2);
        configErrorImage.setVisibility(View.INVISIBLE);
        logErrorImage.setVisibility(View.INVISIBLE);
        addConfigImageButton = (ImageButton) findViewById(R.id.imageButtonAddConfigFile);
        addLogImageButton = (ImageButton) findViewById(R.id.imageButtonAddLogFile);
        seekBarSpeed = (SeekBar) findViewById(R.id.seekBarSpeed);
        seekBarTime = (SeekBar) findViewById(R.id.seekBarTime);
        seekBarTail = (SeekBar) findViewById(R.id.seekBarTail);

        bottonImageButton_play = (Button) findViewById(R.id.bottonImageButton_play);
        bottonImageButton_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ButtonStartPressed(v);
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonStartPressed(view);
            }
        });
    }

    /**
     * Opens a FileDialog where user can choose a config file.
     * Checks if configfile is valid.
     * Click event is bound in layout.
     * @param view Parameter of view which is calling.
     */
    public void chooseConfigFile(View view) {
        String storage = System.getenv("EXTERNAL_STORAGE");
        File mPath = new File(storage);// + "//DIR//");
        FileDialog fileDialog = new FileDialog(this, mPath, CONSTANT.CONFIGFILE_FORMAT);
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            public void fileSelected(File file) {
                Log.d(getClass().getName(), "selected configfile " + file.toString());
                labelConfigFile.setText(file.toString());
                configIsValidated = ConfigReader.testInputData(file.toString());
                configErrorImage.setVisibility(configIsValidated ? View.INVISIBLE : View.VISIBLE);
            }
        });
        fileDialog.showDialog();
    }

    /**
     * Opens a FileDialog where user can choose a config file.
     * Checks if configfile is valid.
     * Click event is bound in layout.
     * @param view Parameter of view which is calling.
     */
    public void chooseLogFile(View view) {
        String storage = System.getenv("EXTERNAL_STORAGE");
        File mPath = new File(storage);
        FileDialog fileDialog = new FileDialog(this, mPath, CONSTANT.LOGFILE_FORMAT2);
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            public void fileSelected(File file) {
                Log.d(getClass().getName(), "selected logfile " + file.toString());
                labelLogFile.setText(file.toString());
                logIsValidated = LogReader.testInputDataAsString(file.toString());
                logErrorImage.setVisibility(logIsValidated ? View.INVISIBLE : View.VISIBLE);
            }
        });
        fileDialog.showDialog();
    }

    /**
     * Called when the start button is pressed.
     * Checks validation and starts MapFullScreenActivity if validation is correct.
     * Otherwise it shows a Snackbar which tells the user to change his input.
     * @param view
     */
    public void ButtonStartPressed(View view) {
        if (!checkValidation()) {
            Snackbar snackbar = Snackbar.make(view, "Eingaben korrigieren!", Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }

        Intent intent = new Intent(ReplayActivity.this, MapFullscreenActivity.class);

        Bundle b = new Bundle();
        b.putString("configFile", labelConfigFile.getText().toString());
        b.putString("logFile", labelLogFile.getText().toString());
        b.putInt("speed", seekBarSpeed.getProgress());

        Log.d("STARTTIME", String.valueOf(seekBarTime.getProgress()));

        b.putInt("time", seekBarTime.getProgress());
        b.putInt("tail", seekBarTail.getProgress());
        intent.putExtras(b);

        startActivity(intent);
    }

    /**
     * Checks if config file and logfile are validated
     * @return returns true of both are valid. Else returns false.
     */
    private Boolean checkValidation() {
        if (!configIsValidated | labelConfigFile.getText().equals("Config File"))
            return false;
        if (!logIsValidated | labelConfigFile.getText().equals("Log File"))
            return false;
        return true;
    }

    /**
     * Called when activity is paused.
     * Saves validation status of logfile and config file
     * @param outState Bundle to put in values which should be saved
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //make sure to check for null
        if (logIsValidated != null) outState.putBoolean("LOG_VALIDATION", logIsValidated);
        if (configIsValidated != null) outState.putBoolean("CONFIG_VALIDATION", configIsValidated);
    }

    /**
     * Called when Activity is unpaused.
     * Tries to restore validation values and set ErrorImages accordingly.
     * @param savedInstanceState Bundle where were saved before pause.
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            //restore validation values
            //default value = false to prevent crushes
            logIsValidated = savedInstanceState.getBoolean("LOG_VALIDATION", false);
            configIsValidated = savedInstanceState.getBoolean("CONFIG_VALIDATION", false);

            //set error images according to validation
            configErrorImage.setVisibility(configIsValidated ? View.VISIBLE : View.INVISIBLE);
            logErrorImage.setVisibility(logIsValidated ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
