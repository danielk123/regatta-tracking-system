package kopfdaniel.regattatrackingsystem;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import DomainModel.Gps.GpsSender;
import DomainModel.Map.Map;
import Infrastructur.CONSTANT;
import Infrastructur.Communication.BroadcastReceiver.ChangeDataReceiver;
import Infrastructur.Communication.SharedData;

/**
 * This service does the work of drawing objects on a created bitmap.
 * After finishing it sends a broadcast with the new bitmap to MapFullScreenActivity
 * Class Map is used to generate and change the Bitmap and calculate coordinates.
 */
public class DrawBackgroundService extends IntentService {


    Map myMap;
    private int speed, zoom;
    private long runningTime, gpsTimeStart, gpsTimeEnd, gpsTimeStartConst;
    private String status;
    private double runningTimePercentage, time;
    private ChangeDataReceiver mChangedDataReceiver;
    private boolean isStopped;
    private int tail;
    private boolean isBreak;
    private boolean isBlocked;
    private boolean isSleeping;
    private Semaphore semaphore;

    public DrawBackgroundService() {
        super("DrawBackgroundService");
    }

    public DrawBackgroundService(String name) {
        super(name);
    }

    public DrawBackgroundService(String name, int n) {
        super(name);
    }

    /**
     * Called after creating the service.
     *
     * @param workIntent
     */
    @Override
    protected void onHandleIntent(final Intent workIntent) {

        status = "started";
        isStopped = false;
        isBreak = false;
        isSleeping = false;

        // Get data from the incoming Intent
        final Bundle b = workIntent.getExtras();
        final ArrayList<GpsSender> participatingBoots = SharedData.participatingBoots;
        runningTime = (long) b.get("runningTime");
        gpsTimeStart = (long) b.get("gpsTimeStart");
        gpsTimeEnd = (long) b.get("gpsTimeEnd");
        gpsTimeStartConst = (long) b.get("gpsTimeStart");
        speed = (int) b.get("speed");
        tail = (int) b.get("tail");
        time = (int) b.get("time");
        zoom = (int) b.get("zoom");
        runningTimePercentage = (time) / 100;
        semaphore = new Semaphore(1);
        isBlocked = false;

        mChangedDataReceiver = new ChangeDataReceiver(new Handler(), DrawBackgroundService.this) {
            /**
             * Handles broadcasts from MapFullScreenActivity.
             * @param context
             * @param intent
             */
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("CHANGEDDATAECEIVER", "received");
                Bundle b = intent.getExtras();
                String status = b.getString(CONSTANT.EXTENDED_DATA_STATUS);

                if (!status.equals(CONSTANT.CONFIGURATION_CHANGED)) return;
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                //if(isBlocked) return;
                //isBlocked = true;
                //waitTillServiceSleeps();

                getDataFrom(b);
                int xTileUp = b.getInt("xTileUp");
                int yTileUp = b.getInt("yTileUp");
                int newZoom = b.getInt("zoom");

                changeMap(newZoom, xTileUp, yTileUp);

                //waitTillServiceWorks();
                semaphore.release();
            }
        };
        initializeBroadcastReceiver();

        initializeMap(b);
        run(participatingBoots);
    }

    private void waitTillServiceWorks() {
        while (isSleeping) {
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void changeMap(int newZoom, int xTileUp, int yTileUp) {
        if (newZoom != zoom || xTileUp != 0 || yTileUp != 0) {
            myMap.changeSector(xTileUp, yTileUp, newZoom - zoom);
            zoom = newZoom;
            Bitmap bm = myMap.generateBitmap(newZoom);
            myMap.copyBitmap(bm);
        }
    }

    /**
     * Sets values speed and tail from Bundle b
     * @param b Bundle sent from MapFullScreenActivity
     */
    private void getDataFrom(Bundle b) {
        speed = b.getInt("speed");
        tail = b.getInt("tail");
    }

    /**
     * Blocks Thread till service sleeps
     */
    private void waitTillServiceSleeps() {
        while (!isSleeping) {
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Used to initialize Map and generate used Bitmap.
     * @param b
     */
    private void initializeMap(Bundle b) {
        double[] mapCoords = (double[]) b.get("mapCoords");
        myMap = new Map(this.getResources());
        myMap.setupMap(mapCoords, zoom);

        Bitmap mergedBitmap = myMap.generateBitmap(zoom);
        myMap.copyBitmap(mergedBitmap);
    }

    /**
     * Loops over all GPS-Times of the logfile.
     * Draws new position for all Boots and sends new bitmap per broadcast to MapFullScreenActivity
     * @param participatingBoots List of All subclasses of GpsSender which send a gps signal
     */
    private void run(ArrayList<GpsSender> participatingBoots) {
        int positionCount = 0;

        if (runningTimePercentage != 0)
            updateRunningTime();

        for (long i = gpsTimeStart; (i < gpsTimeEnd & !isStopped); i = i + 1000) {
            if (isBreak) {
                myMap.recycle();
                break;
            }
            try {
                myMap.drawNewPositions(participatingBoots, i, tail);
                status = CONSTANT.FINISHED;
            } catch (IndexOutOfBoundsException e) {
                Log.d("DRAWBACKGROUNDSERVICE", "Position: " + positionCount + " wasn't saved for a specific boat");
                Log.d("DRAWBACKGROUNDSERVICE", "IndexOutOfBoundsException: " + e.getMessage());
                status = CONSTANT.ERROR_OCCURRED;
                e.printStackTrace();
            } catch (RuntimeException e) {
                e.printStackTrace();
                //continue;
            }
            Intent localIntent =
                    new Intent(CONSTANT.BROADCAST_ACTION)
                            // Puts the status into the Intent
                            .putExtra(CONSTANT.EXTENDED_DATA_STATUS, status);
            localIntent.putExtra("bitmap", myMap.getEmptyMutableBitmap());
            localIntent.putExtra("actualRuntime", i);
            // Broadcasts the Intent to receivers in this app.
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
            positionCount++;
            try {
                isSleeping = true;
                Thread.sleep(1000 - (speed * 100) - 25);
                isSleeping = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initialises BroadcastReceiver to listen to Broadcasts from MapFullScreenActivity.
     */
    private void initializeBroadcastReceiver() {
        IntentFilter mStatusIntentFilter = new IntentFilter(
                CONSTANT.BROADCAST_ACTION);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mChangedDataReceiver,
                mStatusIntentFilter);
    }

    /**
     * Called when onHandleIntent(Intent workIntent) terminates or Service stops
     * because of other reasons like Android kills the service or stopself() is called
     */
    @Override
    public void onDestroy() {
        isStopped = true;
        super.onDestroy();
    }

    public void setTime(int time) {
        runningTimePercentage = time / 100;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setTail(int tail) {
        this.tail = tail;
    }

    /**
     * Changes initial time of loop according to "time"-parameter.
     * runningTimePercentage = time/100, equals time in percent
     */
    private void updateRunningTime() {
        gpsTimeStart += ((int) (gpsTimeEnd - gpsTimeStart) * runningTimePercentage);
        //round milliseconds back to seconds
        gpsTimeStart = ((int) (gpsTimeStart / 1000)) * 1000;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }
}


