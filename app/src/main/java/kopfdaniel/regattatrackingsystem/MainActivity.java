package kopfdaniel.regattatrackingsystem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import Infrastructur.Communication.BroadcastReceiver.ResponseReceiver;

/**
 * Landing page when app starts.
 * Starts WriteImageOnDiskService on first run of the app
 */
public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "swipingTest";
    SharedPreferences prefs = null;
    Intent mServiceIntent;
    private ResponseReceiver mResponseReceiver;

    /**
     * Called when Activity is started.
     * Initializes ResponseReceiver to receive broadcast messages from WriteImagesOnDiskService
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = getSharedPreferences("com.kopfdaniel.regatta-tracking-system", MODE_PRIVATE);

        mResponseReceiver = new ResponseReceiver(new Handler(), MainActivity.this) {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("RESPONSERECEIVER", "received");

                prefs.edit().putBoolean("firstrun", false).commit();
                Toast.makeText(context, "Assets copied to sdcard finished", Toast.LENGTH_LONG).show();
                stopService(mServiceIntent);
            }
        };

        if (prefs.getBoolean("firstrun", true)) startWriteImagesOnDiskService();
    }

    /**
     * Starts ReplayActivity if the user swipes.
     * @param event MotionEvent
     * @return returns true is event is consumed.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                Log.d(DEBUG_TAG, "Action was DOWN");
                return true;
            case (MotionEvent.ACTION_MOVE):
                Log.d(DEBUG_TAG, "Action was MOVE");
                Intent intent = new Intent(MainActivity.this, ReplayActivity.class);
                startActivity(intent);
                return true;
            case (MotionEvent.ACTION_UP):
                Log.d(DEBUG_TAG, "Action was UP");
                return true;
            case (MotionEvent.ACTION_CANCEL):
                Log.d(DEBUG_TAG, "Action was CANCEL");
                return true;
            case (MotionEvent.ACTION_OUTSIDE):
                Log.d(DEBUG_TAG, "Movement occurred outside bounds " +
                        "of current screen element");
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    /**
     * Called when app was paused and starts again.
     * Starts WriteImagesOnDiskService on first run
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("firstrun", false)) {
            startWriteImagesOnDiskService();
        }
    }

    /**
     * Starts WriteImagesOnDiskService
     */
    private void startWriteImagesOnDiskService() {
        Toast.makeText(this, "Starting to copy Assets to sdcard", Toast.LENGTH_LONG).show();
        mServiceIntent = new Intent(this, WriteImagesOnDiskService.class);
        this.startService(mServiceIntent);

    }
}
