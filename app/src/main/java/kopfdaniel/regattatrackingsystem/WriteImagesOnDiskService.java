package kopfdaniel.regattatrackingsystem;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import Infrastructur.CONSTANT;

/**
 * Created by Daniel-PC on 11.05.2017.
 */

public class WriteImagesOnDiskService extends IntentService {

    /**
     * Creates an IntentService.
     * @param name Used to name the worker thread. Only for debugging import.
     */
    public WriteImagesOnDiskService(String name) {
        super(name);
    }

    public WriteImagesOnDiskService() {
        super("DrawBackgroundService");
    }

    /**
     * Writes all files to internal storage.
     * Sets SharedPrefercen "firstrun" false after finishing
     *
     * @param intent
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        copyFilesToSdCard();
        SharedPreferences prefs;
        prefs = getSharedPreferences("com.kopfdaniel.regatta-tracking-system", MODE_PRIVATE);
        prefs.edit().putBoolean("firstrun", false).commit();
    }

    /**
     * Copy all files in assets folder in projekt folder on internal storage.
     */
    private void copyFilesToSdCard() {
        copyFileOrDir("");
    }

    /**
     * Copies file or directory to internal storage.
     * @param path
     */
    private void copyFileOrDir(String path) {
        AssetManager assetManager = this.getAssets();
        String assets[] = null;
        try {
            Log.i("tag", "copyFileOrDir() " + path);
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath = CONSTANT.TARGET_BASE_PATH + path;
                Log.i("tag", "path=" + fullPath);
                File dir = new File(fullPath);
                if (!dir.exists() && !path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                    if (!dir.mkdirs())
                        Log.i("tag", "could not create dir " + fullPath);
                for (int i = 0; i < assets.length; ++i) {
                    String p;
                    if (path.equals(""))
                        p = "";
                    else
                        p = path + "/";

                    if (!path.startsWith("images") && !path.startsWith("sounds") && !path.startsWith("webkit"))
                        copyFileOrDir(p + assets[i]);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    /**
     * Copies file to internal storage.
     * @param filename
     */
    private void copyFile(String filename) {
        AssetManager assetManager = this.getAssets();

        InputStream in = null;
        OutputStream out = null;
        String newFileName = null;
        try {
            Log.i("tag", "copyFile() " + filename);
            in = assetManager.open(filename);
            if (filename.endsWith(".jpg")) // extension was added to avoid compression on APK file
                newFileName = CONSTANT.TARGET_BASE_PATH + filename.substring(0, filename.length() - 4);
            else
                newFileName = CONSTANT.TARGET_BASE_PATH + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", "Exception in copyFile() of " + newFileName);
            Log.e("tag", "Exception in copyFile() " + e.toString());
        }
    }
}
