package kopfdaniel.regattatrackingsystem;

import android.support.multidex.MultiDexApplication;

/**
 * Created by Daniel-PC on 14.05.2017.
 */

/**
 * Used to support MultiDexing
 */
public class ApplicationClass extends MultiDexApplication {
}
