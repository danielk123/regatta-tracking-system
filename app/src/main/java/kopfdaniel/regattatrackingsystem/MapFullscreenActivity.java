package kopfdaniel.regattatrackingsystem;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import DomainModel.Gps.Boot;
import DomainModel.Gps.BootColor;
import DomainModel.Gps.GpsSender;
import DomainModel.Gps.GpsSignal;
import Infrastructur.CONSTANT;
import Infrastructur.Communication.BroadcastReceiver.ResponseReceiver;
import Infrastructur.Communication.SharedData;
import Infrastructur.ConfigReader;
import Infrastructur.LogReader;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MapFullscreenActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private static Bundle b;
    private final Handler mHideHandler = new Handler();
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    protected Toolbar toolbar;
    ArrayList<GpsSignal> smList;
    ArrayList<GpsSender> participatingBoots = new ArrayList<>();
    ResponseReceiver mResponseReceiver;
    private Intent mServiceIntent;
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        }
    };
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    private int speed, time, tail, zoom;
    private long runningTime, actualRuntime, firstGpsTime, lastGpsTime;
    private double[] mapCoords;
    private ScaleGestureDetector mScaleDetector;
    private GestureDetectorCompat mDetector;
    private float mScaleFactor = 1.f;
    private View mControlsView;
    private boolean mVisible;
    private boolean isScaleBlocked;

    /**
     * Called when Activity is created
     * Initializes BroadcastReceiver, Gesture Detectors and variables according to Bundle savedInstaceState.
     * Calls configureReplay()
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVisible = true;
        isScaleBlocked = false;
        mControlsView = findViewById(R.id.fullscreen_content_controls);

        mScaleDetector = new ScaleGestureDetector(MapFullscreenActivity.this, new ScaleListener());
        mDetector = new GestureDetectorCompat(this, this);

        mResponseReceiver = new ResponseReceiver(new Handler(), MapFullscreenActivity.this) {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("RESPONSERECEIVER", "received");
                actualRuntime = intent.getExtras().getLong("actualRuntime");
                super.onReceive(context, intent);

            }
        };
        initializeBroadcastReceiver();

        b = getIntent().getExtras();
        speed = b.getInt("speed");
        time = b.getInt("time");
        tail = b.getInt("tail");

        configureReplay();
    }

    /**
     * Configures Activity parameter for replay.
     * Sets zoom and reads values from logfile and configfile and
     * creates Boots with GpsSignals from log
     */
    private void configureReplay(){
        //default value
        zoom = 18;
        mapCoords = readConfigFile();
        SharedData.startCoords = mapCoords;
        smList = readLogFile();
        sortList(smList);

        if (smList.size() != 0) {
            firstGpsTime = smList.get(0).getGPStime();
            lastGpsTime = smList.get(smList.size() - 1).getGPStime();
        } else {
            firstGpsTime = 0;
            lastGpsTime = 0;
        }
        participatingBoots = setGpsPositionsFrom(smList);
        runningTime = lastGpsTime - firstGpsTime;

        smList.clear();
        smList = null;
    }

    /**
     * Called when onCreate() finished
     * Starts DrawLineBackgroundService
     * @param savedInstanceState
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
        startDrawLineBackgroundService();
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        //mHideHandler.removeCallbacks(mHideRunnable);
        //mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    private ArrayList<GpsSignal> readLogFile() {
        ArrayList<GpsSignal> smList = new ArrayList<>();

        String logfile = b.getString("logFile");
        try {
            smList = LogReader.readAsString(logfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("READLOGFILE", "end of reading");
        return smList;
    }

    private double[] readConfigFile() {

        String configFile = b.getString("configFile");
        double[] mapCoords;
        ConfigReader reader = new ConfigReader(configFile);
        mapCoords = reader.readCoords();
        participatingBoots = reader.readParticipants();
        GpsSender finishBuoy = reader.readStartLineBuoys();
        participatingBoots.add(finishBuoy);
        ArrayList<GpsSender> buoys = reader.readBuoys();
        for (GpsSender boot :
                buoys) {
            participatingBoots.add(boot);
        }

        reader = null;
        Log.d("READCONFIGFILE", "end of reading");
        return mapCoords;
    }

    private void sortList(ArrayList<GpsSignal> smList) {
        Collections.sort(smList, new Comparator<GpsSignal>() {
            public int compare(GpsSignal gpsSignal1, GpsSignal gpsSignal2) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    return Long.compare(gpsSignal1.getGPStime(), gpsSignal2.getGPStime());
                }

                return gpsSignal1.getGPStime() < gpsSignal2.getGPStime() ? -1 :
                        gpsSignal1.getGPStime() > gpsSignal2.getGPStime() ? 1 : 0;
            }
        });
    }

    private ArrayList<GpsSender> createParticipatingBootsFrom(ArrayList<GpsSignal> smList) {
        List<Integer> allDeviceNumbers = new ArrayList<>();
        ArrayList<GpsSender> participatingBoots = new ArrayList<>();

        for (int i = 0; i < smList.size(); i++) {
            GpsSignal sm = smList.get(i);
            int deviceNumber = sm.device;

            if (!allDeviceNumbers.contains(deviceNumber)) {
                Boot boot = new Boot(BootColor.randomColor(), String.valueOf(deviceNumber), String.valueOf(deviceNumber), deviceNumber);
                allDeviceNumbers.add(deviceNumber);
                participatingBoots.add(boot);
            }
            setGpsPosition(participatingBoots, allDeviceNumbers, sm);
        }
        return participatingBoots;
    }

    private void setGpsPosition(ArrayList<GpsSender> participatingBoots, List<Integer> allDeviceNumbers, GpsSignal sm) {
        participatingBoots.get(allDeviceNumbers.indexOf(sm.device)).newPosition(sm);
    }

    private ArrayList<GpsSender> setGpsPositionsFrom(ArrayList<GpsSignal> smList) {
        if (participatingBoots.size() == 0) {
            return createParticipatingBootsFrom(smList);
        }

        int i = 0;
        while (i < smList.size()) {
            GpsSignal sm = smList.get(i);
            int deviceNumber = sm.device;

            for (GpsSender gpsSender :
                    participatingBoots) {
                if (gpsSender.deviceNumber == deviceNumber) {
                    gpsSender.newPosition(sm);
                    break;
                }
            }
            i++;
        }
        return participatingBoots;
    }

    /**
     * Starts DrawLineBackgroundService.
     * Parameters are added to mServiceIntent
     */
    private void startDrawLineBackgroundService() {
        mServiceIntent = new Intent(this, DrawBackgroundService.class);

        SharedData.participatingBoots = participatingBoots;

        mServiceIntent.putExtra("mapCoords", mapCoords);
        mServiceIntent.putExtra("runningTime", runningTime);
        mServiceIntent.putExtra("gpsTimeStart", firstGpsTime);
        mServiceIntent.putExtra("gpsTimeEnd", lastGpsTime);
        mServiceIntent.putExtra("speed", speed);
        mServiceIntent.putExtra("time", time);
        mServiceIntent.putExtra("tail", tail);
        mServiceIntent.putExtra("zoom", zoom);

        this.startService(mServiceIntent);
    }

    /**
     * Initialises BroadcastReceiver to receive messages from DrawBackgroundService
     */
    private void initializeBroadcastReceiver() {
        IntentFilter mStatusIntentFilter = new IntentFilter(
                CONSTANT.BROADCAST_ACTION);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mResponseReceiver,
                mStatusIntentFilter);
    }

    /**
     * Called User opens option Menu
     * @param menu
     * @return returns true if event is consumed
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.add(R.string.configurationTitle);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setDisplayShowTitleEnabled(false);

        supportInvalidateOptionsMenu();
        return true;
    }

    /**
     * Called when user clicks on a Item of OptionsMenu.
     * Initializes Configuration Dialog where user can change configurations
     * since theit is only one OptionItem item at the moment.
     * @param item Item clicked
     * @return returns true if event is consumed
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MapFullscreenActivity.this);
        //dialogBuilder.setTitle(R.string.configurationTitle);
        LayoutInflater inflater = MapFullscreenActivity.this.getLayoutInflater();
        final View contentView = inflater.inflate(R.layout.configuration_dialog, null);

        final int time = calcProgressBarTime();
        this.time = time;
        setSeekBarProgress(contentView, time);

        dialogBuilder.setView(contentView);
        dialogBuilder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                final int time = ((SeekBar) contentView.findViewById(R.id.dialog_seekBarTime)).getProgress();
                final int tail = ((SeekBar) contentView.findViewById(R.id.dialog_seekBarTail)).getProgress();
                final int speed = ((SeekBar) contentView.findViewById(R.id.dialog_seekBarSpeed)).getProgress();
                adaptBackgroundService(time, tail, speed, zoom);
                dialog.dismiss();
            }
        });

        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        dialogBuilder.show();
        return false;
    }

    /**
     * Calculates the integer value for the time progressbar which represents the current time.
     * @return Returns a value between 0 and 100
     */
    private int calcProgressBarTime() {
        return (int) (((actualRuntime - firstGpsTime) / (double) runningTime) * 100);
    }

    /**
     * Updates time, speed, tail or zoom parameter.
     * Restarts DrawLineBackgroundService when parameter time is changed.
     * Calls notifyBackgroundService when after values then time are changed.
     * @param time Value for running time in percentage (between 0 and 100)
     * @param tail Value for tail (between 0 and 100)
     * @param speed Value for speed (between 0 and 9)
     * @param zoom Value for zoomlevel (only values between 14 and 19 have effect)
     */
    private void adaptBackgroundService(final int time, final int tail, final int speed, final int zoom) {
        if (this.speed != speed | this.tail != tail | this.zoom != zoom) {
            this.speed = speed;
            this.tail = tail;
            this.zoom = zoom;

            notifyBackgroundService(speed, tail, zoom, 0, 0);
        }
        if (this.time != time) {
            this.time = time;

            stopService(mServiceIntent);
            while (isMyServiceRunning(DrawBackgroundService.class)) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            startDrawLineBackgroundService();
        }
    }

    /**
     * Sends a broadcast message to DrawLineBackgroundService which contains changed speed, tail or zoom parameter or
     * Map should be moved by one tile in x or y direction.
     * @param tail Value for tail (between 0 and 100)
     * @param speed Value for speed (between 0 and 9)
     * @param zoom Value for zoomlevel (only values between 14 and 19 have effect)
     * @param dx positive number to move map x tiles to right, negative number to move map x tiles to left
     * @param dy positive number to move map x tiles to down, negative number to move map x tiles to top
     */
    private void notifyBackgroundService(final int speed, final int tail, final int zoom, int dx, int dy) {
        String status = CONSTANT.CONFIGURATION_CHANGED;
        Intent localIntent =
                new Intent(CONSTANT.BROADCAST_ACTION)
                        // Puts the status into the Intent
                        .putExtra(CONSTANT.EXTENDED_DATA_STATUS, status);
        localIntent.putExtra("speed", speed);
        localIntent.putExtra("tail", tail);
        localIntent.putExtra("zoom", zoom);
        localIntent.putExtra("xTileUp", dx);
        localIntent.putExtra("yTileUp", dy);
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    /**
     * Sets values of seekbar in ConfigurationDialog
     * @param contentView Layout of ConfigDialog
     * @param time Time parameter. Calculated by calcProgressBarTime().
     */
    private void setSeekBarProgress(final View contentView, final int time) {
        ((SeekBar) contentView.findViewById(R.id.dialog_seekBarTime)).setProgress(time);
        ((SeekBar) contentView.findViewById(R.id.dialog_seekBarTail)).setProgress(tail);
        ((SeekBar) contentView.findViewById(R.id.dialog_seekBarSpeed)).setProgress(speed);
    }

    /**
     * Called when onHandleIntent(Intent workIntent) terminates or Service stops
     * because of other reasons like Android kills the service or stopself() is called
     */
    @Override
    public void onDestroy() {
        stopService(mServiceIntent);
        super.onDestroy();
    }

    /**
     * Progresses touchEvent on display
     * @param event Motion event
     * @return Retuns true if event is consumed
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDetector.onTouchEvent(event);
        mScaleDetector.onTouchEvent(event);
        //super.onTouchEvent(event);
        return true;
    }

    /**
     * Moves map tilewise
     * @param dx positive number to move map x tiles to right, negative number to move map x tiles to left
     * @param dy postiive number to move map x tiles to down, negative number to move map x tiles to top
     */
    public void adaptTile(int dx, int dy) {
        notifyBackgroundService(speed, tail, zoom, dx, dy);
    }

    /**
     * Checks if a specific service is running.
     * @param serviceClass Class of Service
     * @return Returns true if service is running, else false
     */
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Called when display registeres a touch
     * Returns true since every touch event starts with this event
     * @param e
     * @return Returns always true
     */
    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    /**
     * Does nothing
     * @param e
     */
    @Override
    public void onShowPress(MotionEvent e) {

    }

    /**
     * Doesn nothing
     * @param e
     * @return
     */
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    /**
     * Does nothing
     * @param e1
     * @param e2
     * @param distanceX
     * @param distanceY
     * @return
     */
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    /**
     * Does nothing
     * @param e
     */
    @Override
    public void onLongPress(MotionEvent e) {

    }

    /**
     * Called when display registers a swipe event.
     * Calculates course of the swipe and calls appropriate onSwipeX() method.
     * @param e1 touchevent 1 ("down"). Event when user touches display
     * @param e2 touchevent 2 ("up"). Event when user releases touch
     * @param velocityX
     * @param velocityY
     * @return
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

        Log.d("FLINGCALLED", "fling called");
        final int SWIPE_THRESHOLD = 100;
        final int SWIPE_VELOCITY_THRESHOLD = 100;
        boolean result = false;
        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        onSwipeRight();
                    } else {
                        onSwipeLeft();
                    }
                    result = true;
                }
            } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                if (diffY > 0) {
                    onSwipeBottom();
                } else {
                    onSwipeTop();
                }
                result = true;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    /**
     * Swipe direction was from bottom to top.
     * yTile is increased by 1
     */
    public void onSwipeTop() {
        Toast.makeText(this, "top", Toast.LENGTH_SHORT).show();
        this.adaptTile(0, 1);
    }

    /**
     * Swipe direction was from left to right.
     * xTile is decreased by 1
     */
    public void onSwipeRight() {
        Toast.makeText(this, "right", Toast.LENGTH_SHORT).show();
        this.adaptTile(-1, 0);
    }

    /**
     * Swipe direction was from right to left.
     * xTile is increased by 1
     */
    public void onSwipeLeft() {
        Toast.makeText(this, "left", Toast.LENGTH_SHORT).show();
        this.adaptTile(1, 0);
    }

    /**
     * Swipe direction was from top to bottom.
     * yTile is decreased by 1
     */
    public void onSwipeBottom() {
        Toast.makeText(this, "bottom", Toast.LENGTH_SHORT).show();
        this.adaptTile(0, -1);
    }

    /**
     * Class ScaleListener is used to detect scale events with 2 fingers.
     * These are used to detect changes of zoomlevel by user interaction.
     */
    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        private float startSpan;
        private float endSpan;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return true;
        }

        /**
         * Sets span between 2 fingers at the beginning of the scale event.
         * @param detector
         * @return
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            if (isScaleBlocked) return true;
            isScaleBlocked = true;
            startSpan = detector.getCurrentSpan();
            return true;
        }

        /**
         * Sets span between 2 fingers at the end of the scale event.
         * Adapts zoomlevel according to span.
         * @param detector
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            endSpan = detector.getCurrentSpan();
            Log.d("ONSCALE", "on scale called");
            float mScaleFactor = endSpan - startSpan;
            Log.d("ONSCALE", "mscaleFactor: " + mScaleFactor / 1.0);
            if (mScaleFactor < 0) {
                if (zoom >= 19) {
                    isScaleBlocked = false;
                    return;
                }
                time = calcProgressBarTime();
                adaptBackgroundService(time, tail, speed, zoom + 1);
            }

            if (mScaleFactor > 1) {
                if (zoom <= 14) {
                    isScaleBlocked = false;
                    return;
                }
                time = calcProgressBarTime();
                adaptBackgroundService(time, tail, speed, zoom - 1);
            }
            isScaleBlocked = false;
        }
    }
}
