package Infrastructur.Communication.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import Infrastructur.CONSTANT;
import kopfdaniel.regattatrackingsystem.MapFullscreenActivity;


/**
 * Subclass of BroadcastReceiver. Used to receive Broadcast messages of DrawBackgroundService which contain the drawn bitmap.
 */
public class ResponseReceiver extends BroadcastReceiver {

    // Prevents instantiation

    private final Handler handler; // Handler used to execute code on the UI thread
    private final android.content.Context context;
    public Bitmap drawnBitmap;

    /**
     * Constructor
     *
     * @param handler
     * @param context
     */
    public ResponseReceiver(Handler handler, android.content.Context context) {
        this.handler = handler;
        this.context = context;
    }

    /**
     * Called when a broadcast message is received.
     * @param context
     * @param intent
     */
    public void onReceive(final Context context, final Intent intent) {
        Log.d("ONRECEIVE", "Intent: " + intent.getExtras().toString());
        Bundle b = intent.getExtras();
        String status = b.getString(CONSTANT.EXTENDED_DATA_STATUS);

        if (!status.equals(CONSTANT.FINISHED)) return;

        drawnBitmap = (Bitmap) b.get("bitmap");

        handler.post(new Runnable() {
            @Override
            public void run() {
                updateUI();
            }
        });
    }

    /**
     * Sets received bitmap as View of MapFullScreenActivity.
     * Is executed on UI Thread.
     */
    private void updateUI() {
        MapFullscreenActivity a = (MapFullscreenActivity) context;
        ImageView imgView = new ImageView(a);
        imgView.setImageBitmap(drawnBitmap);
        a.setContentView(imgView);
    }
}

