package Infrastructur.Communication.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import Infrastructur.CONSTANT;
import kopfdaniel.regattatrackingsystem.DrawBackgroundService;


/**
 * Extension of BroadcastReceiver to receive messages with ChangedData.
 * DrawBacklineService uses this class.
 */
public class ChangeDataReceiver extends BroadcastReceiver {

    private final Handler handler;
    private final android.content.Context context;
    private int speed, zoom;

    /**
     * Constructor.
     *
     * @param handler Used to execute code on the UI Thread
     * @param context
     */
    public ChangeDataReceiver(Handler handler, android.content.Context context) {
        this.handler = handler;
        this.context = context;
    }

    /**
     * Called when a broadcast message is received.
     * DrawBackgroundService overrides this method.
     * @param context
     * @param intent
     */
    public void onReceive(final Context context, final Intent intent) {

        Log.d("ONRECEIVE", "Intent: " + intent.getExtras().toString());

        Bundle b = intent.getExtras();
        String status = b.getString(CONSTANT.EXTENDED_DATA_STATUS);

        if (!status.equals(CONSTANT.CONFIGURATION_CHANGED)) return;

        speed = b.getInt("speed");
        zoom = b.getInt("zoom");
        handler.post(new Runnable() {
            @Override
            public void run() {
                updateData();
            }
        });

    }

    /**
     * Calls setter of DrawBackgroundService
     */
    private void updateData() {
        DrawBackgroundService service = (DrawBackgroundService) context;
        service.setSpeed(speed);
        service.setZoom(zoom);
    }
}
