package Infrastructur.Communication;

import java.util.ArrayList;

import DomainModel.Gps.GpsSender;


/**
 * Class with static elements.
 * Whole application has access to this parameters
 */
public class SharedData {
    public static ArrayList<GpsSender> participatingBoots;
    public static double[] startCoords;
    public static double[] endCoordsFinishLine;
}
