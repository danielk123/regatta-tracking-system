package Infrastructur;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import DomainModel.Gps.Boot;
import DomainModel.Gps.BootColor;
import DomainModel.Gps.Buoy;
import DomainModel.Gps.GpsSender;
import Infrastructur.Communication.SharedData;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import static jxl.Workbook.getWorkbook;

/**
 * Class to read configuration of out .xls files
 */
public class ConfigReader {

    public Workbook w;
    private String path;

    /**
     * Constructor. Opens woorbook according to path
     *
     * @param inputFile Path where workbook is stored as string.
     */
    public ConfigReader(String inputFile) {
        this.path = inputFile;
        try {
            w = getWorkbook(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    /**
     * Static method to read map coordinates from file.
     * @param inputFile Path where workbook is stored as string.
     * @return returns lat / lon in a double array.
     */
    public static double[] read(final String inputFile) {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        String[] cellContent = null;
        try {
            w = getWorkbook(inputWorkbook);
            // Get the third sheet
            Sheet sheet = w.getSheet(2);

            double map_lat = Double.parseDouble(sheet.getCell(1, 1).getContents().replace(",", "."));
            double map_lon = Double.parseDouble(sheet.getCell(2, 1).getContents().replace(",", "."));

            double[] mapCoords = new double[2];
            mapCoords[0] = map_lat;
            mapCoords[1] = map_lon;

            w.close();
            return mapCoords;

        } catch (BiffException e1) {
            Log.d("Error reading configs", "BiffException: " + e1);
            e1.printStackTrace();
            return null;
        } catch (IOException e) {
            Log.d("Error reading configs", "IOException: " + e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Static method to validate coordinates from config file.
     * @param inputFile Path where workbook is stored as string.
     * @returns returns true if coordinates could be read. returns false if
     * an error occurred.
     */
    @NonNull
    public static Boolean testInputData(final String inputFile) {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        try {
            w = getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet(2);

            Double.parseDouble(sheet.getCell(1, 1).getContents().replace(",", "."));
            Double.parseDouble(sheet.getCell(2, 1).getContents().replace(",", "."));

            w.close();
            return true;
            //@TODO differentiate between errors for more specific output
        } catch (BiffException e) {
            return false;
        } catch (IOException e) {
            return false;
        } catch (NumberFormatException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Reads coordinates of map from declared workbook.
     * @return returns lat / lon in a double array.
     */
    public double[] readCoords() {
        String[] cellContent = null;
        // Get the third sheet
        Sheet sheet = w.getSheet(2);

        double map_lat = Double.parseDouble(sheet.getCell(1, 1).getContents().replace(",", "."));
        double map_lon = Double.parseDouble(sheet.getCell(2, 1).getContents().replace(",", "."));

        double[] mapCoords = new double[2];
        mapCoords[0] = map_lat;
        mapCoords[1] = map_lon;
        return mapCoords;
    }

    /**
     * Reads participating boots out of config file and generates those boots.
     * @return Returns list of boot which participating in this competition.
     */
    public ArrayList<GpsSender> readParticipants() {
        ArrayList<GpsSender> participatingBoots = new ArrayList<>();
        Sheet sheet = w.getSheet(1);

        String[] cellContent = new String[3];
        for (int j = 1; j < sheet.getRows(); j++) {
            for (int i = 0; i < cellContent.length; i++) {
                Cell cell = sheet.getCell(i, j);
                cellContent[i] = cell.getContents();
            }
            int deviceNumber;
            try {
                deviceNumber = Integer.parseInt(cellContent[2]);
            } catch (Exception e) {
                deviceNumber = 0;
            }
            GpsSender boot = new Boot(BootColor.randomColor(), String.valueOf(cellContent[1]), cellContent[0], deviceNumber);
            participatingBoots.add(boot);
            Arrays.fill(cellContent, null);
        }
        return participatingBoots;
    }

    /**
     * Reads the buoy with the startline out of config file.
     * Sets end coordinate in SharedData accordingly to draw finish line.
     * @return Returns the start / finish line buoy.
     */
    public GpsSender readStartLineBuoys() {
        Sheet sheet = w.getSheet(0);

        String[] cellContent = new String[5];
        for (int i = 2; i < 7; i++) {
            Cell cell = sheet.getCell(i, 1);
            cellContent[i - 2] = cell.getContents();
        }
        int deviceNumber;
        try {
            deviceNumber = Integer.parseInt(cellContent[0]);
        } catch (Exception e) {
            deviceNumber = 0;
        }
        GpsSender buoy = new Buoy(BootColor.buoyColor(), "", "", deviceNumber, true);
        double[] endCoords = new double[2];
        endCoords[0] = Double.parseDouble(cellContent[3].replace(",", "."));
        endCoords[1] = Double.parseDouble(cellContent[4].replace(",", "."));
        SharedData.endCoordsFinishLine = endCoords;

        Arrays.fill(cellContent, null);
        return buoy;
    }

    /**
     * Reads all buoys except the buoy with the startline out of config file.
     * @return Returns a list of all buoys.
     */
    public ArrayList<GpsSender> readBuoys() {
        ArrayList<GpsSender> participatingBuoys = new ArrayList<>();
        Sheet sheet = w.getSheet(0);

        String[] cellContent = new String[4];
        for (int j = 2; j < sheet.getRows(); j++) {
            for (int i = 1; i < 5; i++) {
                Cell cell = sheet.getCell(i, j);
                cellContent[i - 1] = cell.getContents();
            }
            int deviceNumber;
            try {
                deviceNumber = Integer.parseInt(cellContent[1]);
            } catch (Exception e) {
                deviceNumber = 0;
            }
            GpsSender buoy = new Buoy(BootColor.buoyColor(), "", "", deviceNumber, false);
            participatingBuoys.add(buoy);
            Arrays.fill(cellContent, null);
        }
        w.close();
        return participatingBuoys;
    }
}

