package Infrastructur;


import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import DomainModel.Gps.GpsSignal;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import static jxl.Workbook.getWorkbook;

/**
 * Class to read gps signals out of logfile
 */
public class LogReader {

    /**
     * Static method to read all signals in logfile as .xls and parse them to object GpsSignal.
     * Use readAsString() instead.
     *
     * @param inputFile Path to workbook as string. Must be .xls format.
     * @return Returns list of all GpsSignals in logfile.
     * @throws IOException Occurs when Path is invalid or file can't be read.
     */
    @Deprecated
    public static ArrayList<GpsSignal> read(String inputFile) throws IOException {
        ArrayList<GpsSignal> smList = new ArrayList<>();
        File inputWorkbook = new File(inputFile);
        Workbook w;
        String[] cellContent;
        try {
            w = getWorkbook(inputWorkbook);
            // Get the first sheet
            Sheet sheet = w.getSheet(0);

            GpsSignal sm;
            //TODO: cellContent only colunm length?
            cellContent = new String[sheet.getColumns() * (sheet.getRows() - 1)];
            for (int j = 1; j < sheet.getRows(); j++) {
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);

                    cellContent[i] = cell.getContents();
                }

                sm = new GpsSignal();
                sm.setFromLog(cellContent);
                smList.add(sm);
                Arrays.fill(cellContent, null);
            }
        } catch (BiffException e1) {
            Log.d("Error reading Log File", "BiffException: " + e1);
            e1.printStackTrace();
            return smList;
        }
        w.close();
        return smList;
    }

    /**
     * Tests if logfile as .xls has a valid format.
     * Use TestInputDataAsString() instead.
     *
     * @param inputFile
     * @return
     */
    @Deprecated
    public static Boolean testInputData(final String inputFile) {
        File inputWorkbook = new File(inputFile);
        Workbook w;
        try {
            w = getWorkbook(inputWorkbook);
            // Get the first sheet
            Sheet sheet = w.getSheet(0);
            String cellContent[] = new String[sheet.getColumns()];

            int j = 1;
            for (int i = 0; i < sheet.getColumns(); i++) {
                Cell cell = sheet.getCell(i, j);

                cellContent[i] = cell.getContents();
            }

            GpsSignal gpsSignal = new GpsSignal();
            gpsSignal.setFromLog(cellContent);

            w.close();
            return true;
            //@TODO differentiate between errors for more specific output
        } catch (BiffException e) {
            return false;
        } catch (IOException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Static method to read all signals in logfile as .csv / string and parse them to object GpsSignal.
     * @param inputFile Path to file as string. Must be .csv or some other string format.
     * @return Returns list of all GpsSignals in logfile.
     * @throws IOException Occurs when Path is invalid or file can't be read.
     */
    public static ArrayList<GpsSignal> readAsString(final String inputFile) throws IOException {
        File logfile = new File(inputFile);
        ArrayList<GpsSignal> smList = new ArrayList<>();
        if (logfile.exists()) {
            FileInputStream inputStream = new FileInputStream(logfile);

            BufferedReader reader = new BufferedReader((new InputStreamReader(inputStream)));
            String line = null;
            reader.readLine(); //don't read first line
            while ((line = reader.readLine()) != null) {
                String[] content;
                content = line.split(";");

                GpsSignal sm = new GpsSignal();
                sm.setFromLog(content);
                smList.add(sm);
            }
        }
        return smList;
    }

    /**
     * Static method to test logfile for valid data.
     * @param inputFile Path to file as string. Must be .csv or some other string format.
     * @return Returns true if format is valid. Else false is returned.
     */
    public static boolean testInputDataAsString(final String inputFile) {
        try {
            File logfile = new File(inputFile);
            if (logfile.exists()) {
                FileInputStream inputStream = new FileInputStream(logfile);

                BufferedReader reader = new BufferedReader((new InputStreamReader(inputStream)));
                String line = null;
                reader.readLine(); //don't read first line

                String[] content;
                line = reader.readLine();
                content = line.split(";");

                GpsSignal sm = new GpsSignal();
                sm.setFromLog(content);
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
