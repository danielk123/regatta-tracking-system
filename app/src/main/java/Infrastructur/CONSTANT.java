package Infrastructur;

/**
 * Declares some static final values
 */
public final class CONSTANT {

    // Defines a custom Intent action
    public static final String BROADCAST_ACTION =
            "BROADCAST Intent";
    // Defines the key for the status "extra" in an Intent
    public static final String EXTENDED_DATA_STATUS =
            "regattatrackingsystem.android.STATUS";
    // Defines the file format which is shown in the file dialog
    public static final String LOGFILE_FORMAT =
            ".xls";
    public static final String LOGFILE_FORMAT2 =
            ".csv";
    // Defines the file format which is shown in the file dialog
    public static final String CONFIGFILE_FORMAT =
            ".xls";
    //Defines String shown for parent directory in file dialog
    public static final String PARENT_DIRECTORY =
            "..";
    //Defines String for Broadcast Communicator
    public static final String CONFIGURATION_CHANGED =
            "configuration data changed";
    //Defines return String for Broadcast Communicator
    public static final String FINISHED =
            "finished";
    //Defines String for unknown Errors
    public static final String ERROR_OCCURRED =
            "one or more errors occurred";
    //Defines Path to Assets
    public final static String TARGET_BASE_PATH =
            System.getenv("EXTERNAL_STORAGE") + "/RegattaTrackingSystem/Assets/";
    //Defines Number of xTiles which will build the map
    public final static int xTiles =
            4;
    //Defines Number of yTiles which will build the map
    public final static int yTiles =
            6;
    //Defines Number of repeats which a boot / buoy should do to get the last gpsTime
    public final static int repeats =
            60;
}
